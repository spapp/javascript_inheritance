/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2016
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   javascript_inheritance
 * @since     2016-09-04
 */

/**
 * Vehicle.Van class
 *
 * @param {string} color
 * @param {number} persons
 * @param {string} brand
 * @constructor
 */
Vehicle.Van = function Vehicle_Van(color, persons, brand) {
    console.log('Vehicle_Van.constructor', arguments);

    // call parent class constructor
    Vehicle.Van.parent.call(this, color, Vehicle.FUEL_DIESEL, persons, brand);
};

Function.inherit(Vehicle.Van, Vehicle);
