/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2016
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   javascript_inheritance
 * @since     2016-09-04
 */

/**
 * Vehicle.Motorcycle class
 *
 * @param {string} color
 * @param {number} persons
 * @param {string} brand
 * @constructor
 */
Vehicle.Motorcycle = function Vehicle_Motorcycle(color, persons, brand) {
    console.log('Vehicle_Motorcycle.constructor', arguments);

    // call parent class constructor
    Vehicle.Motorcycle.parent.call(this, color, Vehicle.FUEL_PETROL, persons, brand);
};

Function.inherit(Vehicle.Motorcycle, Vehicle);
