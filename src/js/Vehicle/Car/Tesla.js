/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2016
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   javascript_inheritance
 * @since     2016-09-04
 */

/**
 * Vehicle.Car.Tesla class
 *
 * @param {string} color
 * @constructor
 */
Vehicle.Car.Tesla = function Vehicle_Car_Tesla(color) {
    var self = Vehicle.Car.Tesla;

    console.log('Vehicle_Car_Tesla.constructor', arguments);

    // call parent class constructor
    self.parent.call(this, color, Vehicle.FUEL_ELECTRIC, 5, self.TYPE_NAME, '');
}

Function.inherit(Vehicle.Car.Tesla, Vehicle.Car);

/**
 * Brand name
 *
 * @type {string}
 */
Vehicle.Car.Tesla.TYPE_NAME = 'Tesla';
