/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2016
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   javascript_inheritance
 * @since     2016-09-04
 */

/**
 * Vehicle.Car.Nissan class
 *
 * @param {string} color
 * @param {string} fuel
 * @param {number} persons
 * @param {string} type
 * @constructor
 */
Vehicle.Car.Nissan = function Vehicle_Car_Nissan(color, fuel, persons, type) {
    var self = Vehicle.Car.Nissan;

    console.log('Vehicle_Car_Nissan.constructor', arguments);

    // call parent class constructor
    self.parent.call(this, color, fuel, persons, self.TYPE_NAME, type);
}

Function.inherit(Vehicle.Car.Nissan, Vehicle.Car);

/**
 * Brand name
 *
 * @type {string}
 */
Vehicle.Car.Nissan.TYPE_NAME = 'Nissan';
