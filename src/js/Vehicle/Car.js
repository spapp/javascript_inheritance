/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2016
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   javascript_inheritance
 * @since     2016-09-04
 */

/**
 * Vehicle.Car class
 *
 * @param {string} color
 * @param {string} fuel
 * @param {number} persons
 * @param {string} brand
 * @param {string} type
 * @constructor
 */
Vehicle.Car = function Vehicle_Car(color, fuel, persons, brand, type) {
    console.log('Vehicle_Car.constructor', arguments);

    // call parent class constructor
    Vehicle.Car.parent.call(this, color, fuel, persons, brand);

    this.type = type;
};

Function.inherit(Vehicle.Car, Vehicle);

/**
 * Car type
 *
 * @type {null}
 */
Vehicle.Car.prototype.type = null;

/**
 * @inheritDoc
 * @type {string[]|string}
 */
Vehicle.Car.prototype.tpl = [
    '{constructorName}: {color} {brand} ({type}) {fuel} {persons} person(s)'
];

/**
 * @inheritDoc
 * @returns {string}
 */
Vehicle.Car.prototype.getTplData = function Vehicle_Car_prototype_getTplData() {
    var tplData = Vehicle.Car.parent.prototype.getTplData.call(this);

    tplData.type = this.type;

    return tplData;
};

