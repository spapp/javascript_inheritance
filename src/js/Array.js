/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2016
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   javascript_inheritance
 * @since     2016-09-05
 */

if (!Array.isArray) {
    Array.isArray = function Array_isArray(arrayObject) {
        return Object.prototype.toString.call(arrayObject) === '[object Array]';
    };
}
