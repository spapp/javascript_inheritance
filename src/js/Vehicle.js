/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2016
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   javascript_inheritance
 * @since     2016-09-04
 */

/**
 * Vehicle class
 *
 * @param {string} color
 * @param {string} fuel
 * @param {number} persons
 * @param {string} brand
 * @constructor
 */
function Vehicle(color, fuel, persons, brand) {
    console.log('Vehicle.constructor', arguments);

    this.color   = color;
    this.fuel    = fuel;
    this.persons = persons;
    this.brand   = brand;
}

/**
 * @constant
 * @type {string}
 */
Vehicle.FUEL_PETROL = 'petrol';

/**
 * @constant
 * @type {string}
 */
Vehicle.FUEL_DIESEL = 'diesel';

/**
 * @constant
 * @type {string}
 */
Vehicle.FUEL_ELECTRIC = 'electric';

/**
 * Vehicle color
 * @type {string}
 */
Vehicle.prototype.color = null;

/**
 * Vehicle fuel type
 * @see {@link #FUEL_PETROL}
 * @see {@link #FUEL_DIESEL}
 * @see {@link #FUEL_ELECTRIC}
 * @type {string}
 */
Vehicle.prototype.fuel = null;

/**
 * Maximum seating capacity
 * @type {string}
 */
Vehicle.prototype.persons = null;

/**
 * Vehicle brand name
 * @type {string}
 */
Vehicle.prototype.brand = null;

/**
 * Template for {@link #toString}
 * @type {string[]|string}
 */
Vehicle.prototype.tpl = [
    '{constructorName}: {color} {brand} {fuel} {persons} person(s)'
];

/**
 * Returns template data for {@link #toString}
 *
 * @see {@link #tpl}
 * @returns {{color: *, brand: *, fuel: *, persons: *}}
 */
Vehicle.prototype.getTplData = function Vehicle_prototype_getTplData() {
    return {
        color : this.color,
        brand : this.brand,
        fuel : this.fuel,
        persons : this.persons,
        constructorName : this.constructor.name
    };
};

/**
 * Returns vehicle as a string
 *
 * @returns {string}
 */
Vehicle.prototype.toString = function Vehicle_prototype_toString() {
    var tpl     = Array.isArray(this.tpl) ? this.tpl.join('') : this.tpl,
        tplData = this.getTplData(),
        name;

    for (name in tplData) {
        tpl = tpl.replace('{' + name + '}', tplData[name]);
    }

    return tpl;
};


