/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2016
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   javascript_inheritance
 * @since     2016-09-04
 */

/**
 * A possible solution how to implement the inheritance
 *
 * @param {Function} baseClass
 * @param {Function} parentClass
 */
Function.inherit = function Function_inherit(baseClass, parentClass) {
    baseClass.parent                = parentClass;
    baseClass.prototype             = Object.create(parentClass.prototype);
    baseClass.prototype.constructor = baseClass;
};


